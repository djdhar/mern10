const express = require('express');
const axios = require('axios');
const redis = require('redis');

const redisClient = redis.createClient(6379);
const app = express();

const MOCK_API = "https://jsonplaceholder.typicode.com/users/";

const PORT = process.env.PORT || 3001;

(async () => {
    await redisClient.connect();
})();

redisClient.on('connect', () => console.log('::> Redis Client Connected'));
redisClient.on('error', (err) => console.log('<:: Redis Client Error', err));

app.listen(PORT, () => {
  console.log(`Server started at port: ${PORT}`);
});

app.get('/user/:email', function(req, res) {
    const email = req.params.email;
    console.log(email)
    axios.get(`${MOCK_API}?email=${email}`)
    .then((response) => {
        res.status(200).send(response.data);
    }).catch(error => {
      res.status(500).send(error)
    });

})

app.get('/cache/user/:email', async function(req, res)  {
    const email = req.params.email;
    redisClient.get(email).then((response) => {
        if(response == null) throw new Error("value not found")
        const user = JSON.parse(response);
        res.send(user)
    }).catch((error) => {
        axios.get(`${MOCK_API}?email=${email}`).then((response) => {
            console.log(response.data)
            const user = response.data
            redisClient.setEx(email, 600, JSON.stringify(user)).finally(()=> {
                res.status(200).send(user); 
            })   
        }).catch(error => {
            res.status(500).send(error);
        })
    })
  })